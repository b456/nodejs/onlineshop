const express = require('express')
// const morgan =  ('morgan')
const app = express()
const { sequelize } = require('../src/models')
const multer = require('multer')
const cloudinary = require('cloudinary')
const fs = require('fs')

const storage = multer.diskStorage({
    destination: function(req,file,callback){
        callback(null, './src/uploads')
    },
    filename: function(req, file, callback){
        callback(null, file.originalname)
    }   
})
const upload = multer ({storage: storage})

cloudinary.config({
    cloud_name: 'dqlhvx6gb',
    api_key: '711138463668271',
    api_secret: 'OBIPQnTnDbvf2Mt0TgVZ6LvV9uU'
})

async function uploadCloudinary(filePath){
    let result;
    try {
        result = await cloudinary.uploader.upload(filePath, {
            use_filename: true})
        fs.unlinkSync(filePath)
        return result.url
    } catch(err){
        fs.unlinkSync(filePath)
        return null
    }
}

// app.use(morgan('combined'))

app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.use('/user', require('./routes/user'))
app.use('/item', require('./routes/item'))
app.post('/profile', upload.single('avatar'), async(req, res)=>{
    // res.send(req.file)
    const url = await uploadCloudinary(req.file.path)
    if (url){
        
        return res.json({
            message: 'upload berhasil',
            url:url,
        })
    } else {
        return res.json({
            message: 'upload gagal',
        })
    }
})

app.post('/photos/upload', upload.array('photos', 12), (req, res)=>{
    res.send(req.files)
})


app.get("/",(req,res)=>{
    console.log("Test root api running")
    return res.send("Test root api running")
})


const connectDb = async ()=>{
    console.log('Checking database connection...')
    try {
        await sequelize.authenticate()
        console.log('Database connection established.')
    } catch (e) {
        console.log('Database connection failed', e)
        process.exit(1)
    }
}

(async ()=> {
    await connectDb()
    app.listen(9000, ()=>{
        console.log("server running 127.0.0.1:9000")
    })

})()

