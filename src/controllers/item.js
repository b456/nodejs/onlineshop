const {User,Item} = require('../models')

exports.createOne = async(req,res,next)=>{
    try {
        Item.create({
            user_id: req.body.user_id,
            name: req.body.name,
            price: req.body.price,
            qty: req.body.qty
        }).then(item => {
            console.log(item)
            return res.status(200).json(item)
        })
    } catch (error) {
        console.log(error)
        return res.status(500).json(error)
    }
}

exports.findAll = async(req,res,next)=>{
    try {
        Item.findAll({
            include: [{
                model: User
            }]
        })
        .then(items => {
            console.log(items)
            return res.status(200).json(items)
        })
    } catch (error) {
        console.log(error)
        return res.status(500).json(error)
    }
}

exports.mine = async(req,res,next)=>{
    try {
        Item.findAll({
            where: {user_id: req.params.user_id},
            include: [{
                model: User
            }]
        })
        .then(items => {
            console.log(items)
            return res.status(200).json(items)
        })
    } catch (error) {
        console.log(error)
        return res.status(500).json(error)
    }
}

exports.updateOne = async(req,res,next)=>{
    try {
        Item.update({
                name: req.body.name,
                price: req.body.price,
                qty: req.body.qty
            },
            {
                where : {id : req.params.id}
            })
            .then(item => {
                console.log(item)
                return res.status(200).json(item)
        })
    } catch (error) {
        console.log(error)
        return res.status(500).json(error)
    }
}