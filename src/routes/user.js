const ctrl = require('../controllers/user')
const router = require('express').Router()

router
    .post('/register', ctrl.register)
    .post('/login',ctrl.login)
    .delete('/delete',ctrl.RemoveAccount)
    .get('/all',ctrl.findAll)

module.exports = router