const ctrl = require('../controllers/item')
const router = require('express').Router()

router
    .post('/create',ctrl.createOne)
    .put('/edit/:id',ctrl.updateOne)
    .get('/all',ctrl.findAll)
    .get('/mine/:user_id',ctrl.mine)

module.exports = router